#include <stdlib.h>
#include <stdio.h>

void make_indentation(int n){
    int i;
    for(i = 0; i < n; i++){
        printf("\t");
    }
}

int power_iteration(int a, int n){

    int i, count = a;

    if(n == 0){
        return 1;
    }

    for (i = 1; i < n; i++)
    {
        count *= a;
    }

    return count;
    
}

int power_recursive(int a, int n,int numero){
    make_indentation(n);
    printf("Appel n°%d\n",numero);
    if(n == 0){
        return 1;
    }
    else{
        return a * power_recursive(a, n-1,numero+1);
    }
    
}

int main(int argc, char const *argv[])
{
    
    int a, n;
    int sortie = 0;

    printf("Enter a number : \n");
    sortie = fscanf(stdin,"%d", &a);
    if(sortie != 1){
        fprintf(stderr, "Erreur : lecture entier impossible !");
        return 1;
    }

    printf("Enter a power : \n");
    sortie = fscanf(stdin,"%d", &n);
    if(sortie != 1){
        fprintf(stderr, "Erreur : lecture entier impossible !");
        return 1;
    }
    fprintf(stdout, "valeur lue : %d puissance %d\n", a,n);
    printf("--- Iteration Function ---\n");
    printf("\tThe result is %d\n", power_iteration(a,n));

    printf("--- Recursive Function ---\n");
    printf("\tThe result is %d\n", power_recursive(a,n,0));

    return 0;
}
