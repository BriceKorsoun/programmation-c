#include <stdio.h>
#include <stdlib.h>

/**
 * Cette fonction realise une recherche dichotomique dans un tableau d'entiers recursivement
 * 
 * @param tableau: C'est le tableau passé en paramètre
 * @param value: Est la valeur a rechercher dans le tableau
 * @param begin: L'indice le plus petit du tableau (il sera toujours égal à 0)
 * @param end: L'indice le plus grand du tableau (taille du tableau - 1)
 * @param size: taille du tableau
 * @return : 1 si l'élément se trouve dans le tableau, 0 sinon
*/
int dichotomie(int tableau[], int value, int begin, int end, int size){

    /* 
        si l'index "begin" (premier element) est égale à l'index "end" (dernier element) 
        on renvoie l'index (un des deux)
    */
    if(begin == end){
        return (tableau[begin] == value);
    }
    if(end == -1){
        end = size-1;
    }
    /* calcule le milieu du tableau et le stocke dans la variable "middle" (il s'agit de l'index )*/
    int middle = (begin + end ) / 2;

    if(tableau[middle] == value){
        /* renvoie l'index "middle" */
        return 1;
    }
    else if(tableau[middle] > value){
        /* rappelle le fonction mais sur la partie gauche du tableau */
        return dichotomie(tableau, value, begin, middle - 1, size);
    }
    else{
        /* rappelle le fonction mais sur la partie droite du tableau */
        return dichotomie(tableau, value, middle + 1, end, size);
    }

}

int main(int argc, char* argv[]){
   
    int tab[10] = { -6, -1, 2, 4, 8, 12, 23, 24, 28, 33};

    int value = atoi(argv[1]);

    printf("%d\n", dichotomie(tab, value, 0, -1, 10));

    return 0;

}