#include <stdio.h>
#include <stdlib.h>

/* Allocate memory for an array which can contain `size`
   integers. The returned C array has memory for an extra last
   integer labelling the end of the array. */
int *allocate_integer_array(int size)
{
  int *new_tab;

  new_tab = (int *)malloc((size + 1) * sizeof(int));
  if (new_tab == NULL)
  {
    fprintf(stderr, "Memory allocation error\n");
    return NULL;
  }
  return new_tab;
}

/* Free an integer array */
void free_integer_array(int *tab)
{
  free(tab);
}

/* Retourne la longueur d'un tableau d'entier passe en argument */
int array_size(int *array)
{

  if (array == NULL)
  {
    printf("Error\n");
    return 0;
  }

  int size;

  for (size = 0; array[size] != -1; size++)
    ;

  return size;
}

/* Affiche le contenu du tableau d'entier passe en arguments */
void print_array(int *array)
{
  if (array == NULL)
  {
    printf("The array is null !\n");
  }
  else
  {
    int index;

    for (index = 0; array[index] != -1; index++)
    {
      if (index == array_size(array) - 1)
        printf("%d", array[index]);
      else
        printf("%d - ", array[index]);
    }
    printf("\n");
  }
}

/* Retourne 1 si les deux tableaux ont le meme contenu et 0 sinon */
int are_arrays_equal(int *first, int *second)
{
  if (first == NULL || second == NULL)
  {
    printf("The array(s) is/are null !\n");
    return 0;
  }
  else if (array_size(first) != array_size(second))
  {
    return 0;
  }
  else
  {
    int index = 0;

    for (index = 0; first[index] == second[index] && first[index] != -1; index++)
      ;

    if (first[index] > second[index] || first[index] < second[index])
    {
      return 0;
    }
    else
    {
      return 1;
    }
  }
}

/* Retourne une nouvelle copie identique du tableau donne en argument */
int *copy_array(int *array)
{
  if (array == NULL)
  {
    return NULL;
  }

  int size = array_size(array);
  int *copy_array = allocate_integer_array(size);
  int index;
  for (index = 0; array[index] != -1; index++)
  {
    copy_array[index] = array[index];
  }

  copy_array[index] = -1;

  return copy_array;
}

/* Demande à l'utilisateur d'entrer en console une longueur puis des entiers positifs
Retourne ensuite un tableau fraichement alloue contenant les entiers donnes.
*/
int *fill_array(void)
{
  int size = 0;
  int *new_array = NULL;
  int index;
  int integer_tmp = 0;

  printf("Entrez la longueur du tableau : \n");
  scanf("%d", &size);

  new_array = allocate_integer_array(size);

  for (index = 0; index < size; index++)
  {
    printf("\nEntrez un entier positif : ");
    scanf("%d", &integer_tmp);
    new_array[index] = integer_tmp;
  }

  new_array[index] = -1;

  return new_array;
}

/* Retourne un nouveau tableau contenant "size" entiers positifs
compris entre 0 et "max_entry"
*/
int *random_array(int size, int max_entry)
{
  int *new_array = allocate_integer_array(size);

  int index;

  for (index = 0; index < size; index++)
  {
    new_array[index] = rand() % max_entry;
  }

  new_array[index] = -1;

  return new_array;
}

/* Retourne un nouveau tableau contenant les entrées concaténées des deux tableaux */
int *concat_array(int *first, int *second)
{
  if (first == NULL || second == NULL)
  {
    printf("L'un des tableaux en argument est null ou les deux\n");
    return NULL;
  }
  else
  {
    int size = array_size(first) + array_size(second);
    int *new_array = allocate_integer_array(size);
    int i, j;

    for (i = 0; first[i] != -1; i++)
    {
      new_array[i] = first[i];
    }
    for (j = 0; second[j] != -1; j++, i++)
    {
      new_array[i] = second[j];
    }

    new_array[i] = -1;
    return new_array;
  }
}

/* Retourne un tableau trié dont les entrées sont exactement
celles des deux tableaux en argument. Ces deux tableaux en arguments
sont supposés déjà triés
*/
int *merge_sorted_arrays(int *first, int *second)
{
  printf("Merge the two following ones :\n");
  print_array(first);
  print_array(second);
  int size_of_first = array_size(first);
  int size_of_second = array_size(second);
  int size = size_of_first + size_of_second;

  int *new_tab = allocate_integer_array(size);
  int index_left = 0, index_right = 0, index_new_tab = 0;

  for (index_new_tab = index_left; index_new_tab <= size; index_new_tab++)
  {
    if (index_left == (size_of_first))
    {
      *(new_tab + index_new_tab) = *(second + index_right);
      index_right++;
    }
    /* si l'on a atteint la limite du second tableau */
    else if (index_right == (size_of_second))
    {
      *(new_tab + index_new_tab) = *(first + index_left);
      index_left++;
    }
    /* si le pointeur du sous-tableau de gauche pointe vers un élément plus petit */
    else if (*(first + index_left) < *(second + index_right))
    {
      *(new_tab + index_new_tab) = *(first + index_left);
      index_left++;
    }
    /* si le pointeur du sous-tableau de droite pointe vers un élément plus petit */
    else
    {
      *(new_tab + index_new_tab) = *(second + index_right);
      index_right++;
    }
  }
  print_array(new_tab);
  return new_tab;
}

/* Cette fonction fabrique deux tableaux (qui seront placés dans les pointeurs)
contenant chacun la moitié des éléments du tableau array en argument
*/
void split_arrays(int *array, int **first, int **second)
{
  printf("Split in array in two part :\n");
  print_array(array);
  /* Calculer la longueur de array */
  int size_array = array_size(array);
  /* Calculer la longueur de first */
  int size_first = size_array / 2;
  /* Fabrication mémoire de first */
  *first = allocate_integer_array(size_first);
  /* Même chose pour second */
  int size_second = size_array - size_first;
  *second = allocate_integer_array(size_second);

  /* Boucle pour remplir first et/ou second */
  int index_array = 0;
  int index_sub_array;

  /* Remplissage de first */
  for (index_sub_array = 0; index_sub_array < size_first; index_sub_array++, index_array++)
  {
    *(*first + index_sub_array) = *(array + index_array);
  }

  /* Dernier élément du tableau a -1 */
  *(*first + index_sub_array) = -1;

  /* Remplissage de second */
  for (index_sub_array = 0; index_sub_array < size_second; index_sub_array++, index_array++)
  {
    *(*second + index_sub_array) = *(array + index_array);
  }

  /* Dernier element du tableau a -1 */
  *(*second + index_sub_array) = -1;

  print_array(*first);
  print_array(*second);
}

/* Algorithme de Tri Fusion */
int *merge_sort(int *array)
{
  if (array == NULL)
  {
    printf("Array is null !\n");
    return NULL;
  }

  if (array_size(array) == 1)
  {
    return array;
  }
  int *new_tab1 = NULL;
  int *new_tab2 = NULL;

  split_arrays(array, &new_tab1, &new_tab2);
  new_tab1 = merge_sort(new_tab1);
  new_tab2 = merge_sort(new_tab2);

  return merge_sorted_arrays(new_tab1, new_tab2);
}

/* An empty main to test the compilation of the allocation and free
   functions. */
int main(int argc, char *argv[])
{

  int *array = random_array(20,100);

  print_array(array);
  array = merge_sort(array);

  printf("Here is the sorted array :\n");
  print_array(array);
  free_integer_array(array);

  return 0;
}

