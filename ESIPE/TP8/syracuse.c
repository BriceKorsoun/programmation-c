#include <stdlib.h>
#include <stdio.h>

#define MAX 30

/* Allocate memory for an array which can contain `size`
   integers. The returned C array has memory for an extra last
   integer labelling the end of the array. */
int *allocate_integer_array(int size)
{
  int *new_tab;

  new_tab = (int *)malloc((size + 1) * sizeof(int));
  if (new_tab == NULL)
  {
    fprintf(stderr, "Memory allocation error\n");
    return NULL;
  }
  return new_tab;
}

/* Free an integer array */
void free_integer_array(int *tab)
{
  free(tab);
}

/**
 * La fonction remplie de tableau de la valeur renseignee
*/
void fill_array(int *tab, int value)
{
  int index;
  for (index = 0; index < MAX; index++)
  {
    tab[index] = value;
  }
}

/**
 * Affiche un tableau
*/
void print_array(int *array)
{
  if (array == NULL)
  {
    printf("The array is null !\n");
  }
  else
  {
    int index;
    printf("[");
    for (index = 0; index < MAX; index++)
    {
      if (index == MAX - 1)
        printf("%d", array[index]);
      else
        printf("%d,", array[index]);
    }
    printf("]\n");
  }
}

/**
 * This function calculate the flight length (is the number of steps needed to reach the value 1).
 * 
 * @param n Starting point
 * @return the flight length
*/
int syracuse(int n)
{
  printf("%d ", n);
  if (n == 1)
  {
    return 0;
  }

  if (n % 2 == 0)
  {
    return 1 + syracuse(n / 2);
  }
  else
  {
    return 1 + syracuse(3 * n + 1);
  }
}

int main(int argc, char const *argv[])
{
  int *tab = allocate_integer_array(MAX);
  fill_array(tab, -1);
  
  int flight_length = syracuse(200);
  printf("\nLongueur de vol est : %d\n", flight_length);

  return 0;
}
