#include <stdio.h>

#include "sudoku.h"

#define N 9

void initialize_empty_board(Board grid)
{
}

void print_board(Board grid)
{
    int i, j;

    for (i = 0; i < N; i++)
    {
        printf("-------------------------------------\n");
        for (j = 0; j < N; j++)
        {
            if (j == 8)
            {
                printf("| %d |", grid[i][j]);
            }
            else
            {
                printf("| %d ", grid[i][j]);
            }
        }
        printf("\n");
        if (i == 8)
        {
            printf("-------------------------------------\n");
        }
    }
}

int isPresent(int *table, int value, int begin, int end, int size)
{
    if (begin == end)
    {
        return (table[begin] == value);
    }
    if (end == -1)
    {
        end = size - 1;
    }
    /* calcule le milieu du table et le stocke dans la variable "middle" (il s'agit de l'index )*/
    int middle = (begin + end) / 2;

    if (table[middle] == value)
    {
        /* renvoie l'index "middle" */
        return 1;
    }
    else if (table[middle] > value)
    {
        /* rappelle le fonction mais sur la partie gauche du table */
        return isPresent(table, value, begin, middle - 1, size);
    }
    else
    {
        /* rappelle le fonction mais sur la partie droite du table */
        return isPresent(table, value, middle + 1, end, size);
    }
}

void resolver(Board grid, int current, int max)
{
    if (current > max)
    {
        return;
    }

    int i, j;
    for (i = 0; i < N; i++)
    {
        for (j = 0; j < N; j++)
        {
            if (!isPresent(grid[i], current, 0, N - 1, N))
            {
                if (grid[i][j] == 0)
                {
                    grid[i][j] = current;
                    resolver(grid, current + 1, max);
                    grid[i][j] = 0;
                }
            }
        }
    }
}
