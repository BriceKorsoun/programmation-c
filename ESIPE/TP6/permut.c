#include <stdlib.h>
#include <stdio.h>

#define N 3

void make_tabulation(int n)
{
  int i;

  for (i = 0; i < n; i++)
  {
    printf("\t");
  }
}

int *allocate_integer_array(int size)
{
  int *new_tab;

  new_tab = (int *)malloc((size + 1) * sizeof(int));
  if (new_tab == NULL)
  {
    fprintf(stderr, "Memory allocation error\n");
    return NULL;
  }
  return new_tab;
}

void free_integer_array(int *tab)
{
  free(tab);
}

void fill_array(int *tab)
{
  int index;
  for (index = 0; index < N; index++)
  {
    tab[index] = 0;
  }
}

void print_array(int *array)
{
  if (array == NULL)
  {
    printf("The array is null !\n");
  }
  else
  {
    int index;
    printf("[");
    for (index = 0; index < N; index++)
    {
      if (index == N - 1)
        printf("%d", array[index]);
      else
        printf("%d,", array[index]);
    }
    printf("]\n");
  }
}

void permutations(int buffer[], int current, int max)
{
  make_tabulation(current - 1);
  print_array(buffer);
  if (current > max)
  {
    return;
  }

  int i;
  for (i = 0; i < N; i++)
  {
    if (buffer[i] == 0)
    {
      buffer[i] = current;
      permutations(buffer, current + 1, max);
      buffer[i] = 0;
    }
  }
}

int main(int argc, char const *argv[])
{
  int *table = allocate_integer_array(N);
  fill_array(table);

  permutations(table, 1, N);

  free(table);
  return 0;
}
