#include <stdlib.h>
#include <stdio.h>

#include "stack.h"

static Stack stack;

void stack_init(void){
    stack.size = 0;
}

/* Returns the current size of the stack. */
int stack_size(void){
    return stack.size;
}

/* Returns 1 if the stack is empty, returns 0 otherwise. */
int stack_is_empty(void){
    return !(stack.size);
}

int stack_top(void){
    return stack.values[stack.size];
}

/* Pops the element at the top of the stack and returns it. */
int stack_pop(void){
    if(stack.size == -1)
        return 1;
    
    int item;
    item = stack.values[stack.size];
    stack.size--;
    return item;
}

/* Pushes a given integer `n` at the top of the stack. */
void stack_push(int n){
    if(stack.size >= MAX_SIZE - 1)
        return;
    stack.size++;
    stack.values[stack.size] = n;
}

/* Displays the content of the stack on the standard output. */
void stack_display(void){
    int i;
    if(stack_is_empty())
        printf("\nLa pile est vide !");
    else{
        for(i = stack.size; i >= 0; i--)
            printf("\n%d", stack.values[i]);
    }
}

/* Returns the element at index `index` inside the stack. The user is
   responsible of the use of this function. The answers may be not
   relevant if a call is done outside the current size of the
   stack. */
int stack_get_element(int index){
    return stack.values[index];
}
