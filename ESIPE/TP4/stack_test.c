#include <stdlib.h>
#include <stdio.h>

#include "stack.h"

int main(int argc, char *argv[])
{
    int item, choice, index;
    int option = 1;

    stack_init();

    printf("\n\tImplémentation d'une pile");
    while (option)
    {
        printf("\nMenu principal");
        printf("\n1.Empiler \n2.Dépiler \n3.Afficher \n4.Afficher un élément \n5.Exit");
        printf("\nEntrez votre choix: ");
        scanf("%d", &choice);
        switch (choice)
        {
        case 1:
            printf("\nEntrez l'élément à empiler: ");
            scanf("%d", &item);
            stack_push(item);
            break;
        case 2:
            stack_pop();
            printf("\nL'élément dépilé est %d", item);
            break;
        case 3:
            stack_display();
            break;
        case 4:
            printf("\nEntrez l'index de l'élément : ");
            scanf("%d", &index);
            item = stack_get_element(index);

            printf("\nL'élément à l'index %d est %d", index, item);
            break;
        case 5:
            exit(0);
            break;
        }
        printf("Voulez-vous continuer (Tapez 0 (Non) ou 1 (Oui)) ? : ");
        scanf("%d", &option);
    }

    return 0;
}