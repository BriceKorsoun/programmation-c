#include <stdio.h>
#include <stdlib.h>

int main(int argc, char const *argv[])
{
    FILE *f;
    char c;

    f = fopen("main.c", "r+");

    while((c=fgetc(f)) != EOF){
        printf("%c",c);
    }

    fclose(f);

    return 0;
}
