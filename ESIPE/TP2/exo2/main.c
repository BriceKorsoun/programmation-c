#include <stdlib.h>
#include <stdio.h>

int main(int argc, char const *argv[])
{
    
    int number1 = atoi(argv[1]);
    int number2 = atoi(argv[2]);

    printf("The sum of this two number is : %d\n", (number1+number2));

    return 0;
}
