#include <stdio.h>

void print_var(int n){
    printf("Value of the variable : %d\n", n);
}

void print_pointer(int* p){
    printf("Pointer address : %p and Pointed value : %d\n", p, *p);
}

void set_pointer(int* p, int n){
    *p = n;
}

int main(int argc, char const *argv[])
{
    int a;
    /* initialise un pointeur sur l'adresse de la variable a */
    int* p = &a;

    /* affiche le valeur contenue à l'adresse de la variable a donc une valeur aléatoire */
    print_var(a);
    /* affecte la valeur 53 à l'adresse de la variable a */
    a = 53;
    /* affiche le valeur contenue à l'adresse de la variable a donc 53*/
    print_var(a);
    /* affiche l'adresse de la variable a et la valeur qu'elle contient */
    print_pointer(p);
    /* affecte la valeur 42 à l'adresse de la variable a */
    set_pointer(p, 42);
    /* affiche l'adresse de la variable a et la valeur qu'elle contient */
    print_pointer(p);
    /* affiche le valeur contenue à l'adresse de la variable a donc 42*/
    print_var(a);


    return 0;
}
