#include <stdlib.h>
#include <stdio.h>

void display(int count){
    if(count != 0){
        printf("%d ", count);
        display(count-1);
        printf("%d ", count);
    }
}

int main(int argc, char const *argv[])
{
    /*  number of iteration  */
    int count1 = atoi(argv[1]);
    /* int count2 = atoi(argv[1]);

    while(count1 >= 1){
        printf("%d ", count1);
        count1--;
    }

    count1++;

    while(count1 <= count2){
        printf("%d ", count1);
        count1++;
    }
    printf("\n"); */

    printf("Fonction recursive : \n");

    display(count1);

    printf("\n");

    return 0;
}
