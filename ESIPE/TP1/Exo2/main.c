#include <stdlib.h>
#include <stdio.h>

/* This program take two integers and calculate the sum of this two integers */

int main(int argc, char const *argv[])
{
    
    int integer1 = atoi(argv[1]);
    int integer2 = atoi(argv[2]);

    printf("The sum of the two integers is %d\n", (integer1+integer2));

    return 0;
}
